package in.jpep.emf.facet.starterkit.examples.internal;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.ui.wizard.ExampleInstallerWizard;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.sirius.ui.tools.api.project.ModelingProjectManager;

public class StarterkitExampleInstallerWizard extends ExampleInstallerWizard {

	private static final String PRJ_PAGE_ID = "projectPage"; //$NON-NLS-1$
	private static final Logger LOG = Logger.getLogger(StarterkitExampleInstallerWizard.class.getName());

	@Override
	public boolean performFinish() {
		boolean result = super.performFinish();
		if (result) {
			final List<ProjectDescriptor> prjDescs = getProjectDescriptors();
			for (ProjectDescriptor prjDesc : prjDescs) {
				final IProject project = prjDesc.getProject();
				try {
					addModelinProjectNature(project);
				} catch (InvocationTargetException exc) {
					final String msg = String.format("Error on convert project %s to modeling nature.", project); //$NON-NLS-1$
					LOG.log(Level.SEVERE, msg, exc);
					result = false;
					final WizardPage page = (WizardPage) getPage(PRJ_PAGE_ID);
					page.setErrorMessage(msg);
				} catch (InterruptedException exc) {
					final String msg = String.format("Converting project %s to modeling nature interrupted by user", //$NON-NLS-1$
							project);
					LOG.log(Level.SEVERE, msg, exc);
				}
			}
		}
		return result;
	}

	private void addModelinProjectNature(final IProject project)
			throws InvocationTargetException, InterruptedException {
		getContainer().run(true, true, new IRunnableWithProgress() {
			@Override
			public void run(final IProgressMonitor monitor) throws InvocationTargetException {
				try {
					ModelingProjectManager.INSTANCE.convertToModelingProject(project, monitor);
				} catch (CoreException exc) {
					throw new InvocationTargetException(exc);
				}
			}
		});
	}

}
