/**
 */
package in.jpep.emf.facet.starterkit.modelset.impl;

import in.jpep.emf.facet.starterkit.modelset.ModelSet;
import in.jpep.emf.facet.starterkit.modelset.ModelsetPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link in.jpep.emf.facet.starterkit.modelset.impl.ModelSetImpl#getModels <em>Models</em>}</li>
 *   <li>{@link in.jpep.emf.facet.starterkit.modelset.impl.ModelSetImpl#getSerialization <em>Serialization</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModelSetImpl extends MinimalEObjectImpl.Container implements ModelSet {
	/**
	 * The cached value of the '{@link #getModels() <em>Models</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModels()
	 * @generated
	 * @ordered
	 */
	protected EList<String> models;

	/**
	 * The default value of the '{@link #getSerialization() <em>Serialization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSerialization()
	 * @generated
	 * @ordered
	 */
	protected static final String SERIALIZATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSerialization() <em>Serialization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSerialization()
	 * @generated
	 * @ordered
	 */
	protected String serialization = SERIALIZATION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelsetPackage.Literals.MODEL_SET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getModels() {
		if (models == null) {
			models = new EDataTypeUniqueEList<String>(String.class, this, ModelsetPackage.MODEL_SET__MODELS);
		}
		return models;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSerialization() {
		return serialization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSerialization(String newSerialization) {
		String oldSerialization = serialization;
		serialization = newSerialization;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelsetPackage.MODEL_SET__SERIALIZATION,
					oldSerialization, serialization));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ModelsetPackage.MODEL_SET__MODELS:
			return getModels();
		case ModelsetPackage.MODEL_SET__SERIALIZATION:
			return getSerialization();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ModelsetPackage.MODEL_SET__MODELS:
			getModels().clear();
			getModels().addAll((Collection<? extends String>) newValue);
			return;
		case ModelsetPackage.MODEL_SET__SERIALIZATION:
			setSerialization((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ModelsetPackage.MODEL_SET__MODELS:
			getModels().clear();
			return;
		case ModelsetPackage.MODEL_SET__SERIALIZATION:
			setSerialization(SERIALIZATION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ModelsetPackage.MODEL_SET__MODELS:
			return models != null && !models.isEmpty();
		case ModelsetPackage.MODEL_SET__SERIALIZATION:
			return SERIALIZATION_EDEFAULT == null ? serialization != null
					: !SERIALIZATION_EDEFAULT.equals(serialization);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (models: ");
		result.append(models);
		result.append(", serialization: ");
		result.append(serialization);
		result.append(')');
		return result.toString();
	}

} //ModelSetImpl
