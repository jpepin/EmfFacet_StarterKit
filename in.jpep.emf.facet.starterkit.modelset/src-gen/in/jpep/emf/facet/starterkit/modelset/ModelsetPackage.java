/**
 */
package in.jpep.emf.facet.starterkit.modelset;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see in.jpep.emf.facet.starterkit.modelset.ModelsetFactory
 * @model kind="package"
 * @generated
 */
public interface ModelsetPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "modelset";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.jpep.in/modelset";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "modelset";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ModelsetPackage eINSTANCE = in.jpep.emf.facet.starterkit.modelset.impl.ModelsetPackageImpl.init();

	/**
	 * The meta object id for the '{@link in.jpep.emf.facet.starterkit.modelset.impl.ModelSetImpl <em>Model Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see in.jpep.emf.facet.starterkit.modelset.impl.ModelSetImpl
	 * @see in.jpep.emf.facet.starterkit.modelset.impl.ModelsetPackageImpl#getModelSet()
	 * @generated
	 */
	int MODEL_SET = 0;

	/**
	 * The feature id for the '<em><b>Models</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_SET__MODELS = 0;

	/**
	 * The feature id for the '<em><b>Serialization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_SET__SERIALIZATION = 1;

	/**
	 * The number of structural features of the '<em>Model Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_SET_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Model Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_SET_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link in.jpep.emf.facet.starterkit.modelset.ModelSet <em>Model Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Set</em>'.
	 * @see in.jpep.emf.facet.starterkit.modelset.ModelSet
	 * @generated
	 */
	EClass getModelSet();

	/**
	 * Returns the meta object for the attribute list '{@link in.jpep.emf.facet.starterkit.modelset.ModelSet#getModels <em>Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Models</em>'.
	 * @see in.jpep.emf.facet.starterkit.modelset.ModelSet#getModels()
	 * @see #getModelSet()
	 * @generated
	 */
	EAttribute getModelSet_Models();

	/**
	 * Returns the meta object for the attribute '{@link in.jpep.emf.facet.starterkit.modelset.ModelSet#getSerialization <em>Serialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Serialization</em>'.
	 * @see in.jpep.emf.facet.starterkit.modelset.ModelSet#getSerialization()
	 * @see #getModelSet()
	 * @generated
	 */
	EAttribute getModelSet_Serialization();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ModelsetFactory getModelsetFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link in.jpep.emf.facet.starterkit.modelset.impl.ModelSetImpl <em>Model Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see in.jpep.emf.facet.starterkit.modelset.impl.ModelSetImpl
		 * @see in.jpep.emf.facet.starterkit.modelset.impl.ModelsetPackageImpl#getModelSet()
		 * @generated
		 */
		EClass MODEL_SET = eINSTANCE.getModelSet();

		/**
		 * The meta object literal for the '<em><b>Models</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_SET__MODELS = eINSTANCE.getModelSet_Models();

		/**
		 * The meta object literal for the '<em><b>Serialization</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_SET__SERIALIZATION = eINSTANCE.getModelSet_Serialization();

	}

} //ModelsetPackage
