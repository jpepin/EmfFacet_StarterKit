/**
 */
package in.jpep.emf.facet.starterkit.modelset;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link in.jpep.emf.facet.starterkit.modelset.ModelSet#getModels <em>Models</em>}</li>
 *   <li>{@link in.jpep.emf.facet.starterkit.modelset.ModelSet#getSerialization <em>Serialization</em>}</li>
 * </ul>
 *
 * @see in.jpep.emf.facet.starterkit.modelset.ModelsetPackage#getModelSet()
 * @model
 * @generated
 */
public interface ModelSet extends EObject {
	/**
	 * Returns the value of the '<em><b>Models</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Models</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Models</em>' attribute list.
	 * @see in.jpep.emf.facet.starterkit.modelset.ModelsetPackage#getModelSet_Models()
	 * @model
	 * @generated
	 */
	EList<String> getModels();

	/**
	 * Returns the value of the '<em><b>Serialization</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Serialization</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Serialization</em>' attribute.
	 * @see #setSerialization(String)
	 * @see in.jpep.emf.facet.starterkit.modelset.ModelsetPackage#getModelSet_Serialization()
	 * @model
	 * @generated
	 */
	String getSerialization();

	/**
	 * Sets the value of the '{@link in.jpep.emf.facet.starterkit.modelset.ModelSet#getSerialization <em>Serialization</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Serialization</em>' attribute.
	 * @see #getSerialization()
	 * @generated
	 */
	void setSerialization(String value);

} // ModelSet
