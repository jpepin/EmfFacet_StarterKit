/**
 */
package in.jpep.emf.facet.starterkit.modelset;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see in.jpep.emf.facet.starterkit.modelset.ModelsetPackage
 * @generated
 */
public interface ModelsetFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ModelsetFactory eINSTANCE = in.jpep.emf.facet.starterkit.modelset.impl.ModelsetFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Model Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model Set</em>'.
	 * @generated
	 */
	ModelSet createModelSet();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ModelsetPackage getModelsetPackage();

} //ModelsetFactory
