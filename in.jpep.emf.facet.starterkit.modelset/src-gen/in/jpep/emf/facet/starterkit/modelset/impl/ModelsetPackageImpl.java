/**
 */
package in.jpep.emf.facet.starterkit.modelset.impl;

import in.jpep.emf.facet.starterkit.modelset.ModelSet;
import in.jpep.emf.facet.starterkit.modelset.ModelsetFactory;
import in.jpep.emf.facet.starterkit.modelset.ModelsetPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ModelsetPackageImpl extends EPackageImpl implements ModelsetPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelSetEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see in.jpep.emf.facet.starterkit.modelset.ModelsetPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ModelsetPackageImpl() {
		super(eNS_URI, ModelsetFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ModelsetPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ModelsetPackage init() {
		if (isInited)
			return (ModelsetPackage) EPackage.Registry.INSTANCE.getEPackage(ModelsetPackage.eNS_URI);

		// Obtain or create and register package
		ModelsetPackageImpl theModelsetPackage = (ModelsetPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof ModelsetPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new ModelsetPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theModelsetPackage.createPackageContents();

		// Initialize created meta-data
		theModelsetPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theModelsetPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ModelsetPackage.eNS_URI, theModelsetPackage);
		return theModelsetPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModelSet() {
		return modelSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModelSet_Models() {
		return (EAttribute) modelSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModelSet_Serialization() {
		return (EAttribute) modelSetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelsetFactory getModelsetFactory() {
		return (ModelsetFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		modelSetEClass = createEClass(MODEL_SET);
		createEAttribute(modelSetEClass, MODEL_SET__MODELS);
		createEAttribute(modelSetEClass, MODEL_SET__SERIALIZATION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(modelSetEClass, ModelSet.class, "ModelSet", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getModelSet_Models(), ecorePackage.getEString(), "models", null, 0, -1, ModelSet.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getModelSet_Serialization(), ecorePackage.getEString(), "serialization", null, 0, 1,
				ModelSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //ModelsetPackageImpl
