/**
 * Copyright (c) 2017 Jonathan Pepin
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Jonathan Pepin - initial API and implementation
 */
package in.jpep.emf.facet.starterkit.editor.internal.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.facet.util.core.Logger;
import org.eclipse.emf.facet.util.ui.internal.exported.handler.HandlerUtils;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;

import in.jpep.emf.facet.starterkit.editor.internal.Activator;

public class ShowPropertiesHandler extends AbstractHandler {

	private static final String PROPSHEET_VIEW_ID = "org.eclipse.ui.views.PropertySheet"; //$NON-NLS-1$

	public Object execute(final ExecutionEvent event) throws ExecutionException {
		try {
			final IWorkbenchPage page = HandlerUtils.getActivePage();
			page.showView(PROPSHEET_VIEW_ID);
		} catch (PartInitException exception) {
			Logger.logError(exception, Activator.getDefault());
		}
		return null;
	}

}
