/**
 * Copyright (c) 2017 Jonathan Pepin
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Jonathan Pepin - initial API and implementation
 */
package in.jpep.emf.facet.starterkit.editor.internal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.ui.dialogs.ResourceDialog;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.emf.edit.ui.util.EditUIUtil;
import org.eclipse.emf.facet.custom.core.ICustomizationManager;
import org.eclipse.emf.facet.custom.core.ICustomizationManagerFactory;
import org.eclipse.emf.facet.custom.core.ICustomizationManagerProvider;
import org.eclipse.emf.facet.custom.edit.ui.IEditingDomainPropertySheetPageFactory;
import org.eclipse.emf.facet.custom.ui.CustomizedContentProviderUtils;
import org.eclipse.emf.facet.custom.ui.ICustomizedContentProviderFactory;
import org.eclipse.emf.facet.custom.ui.ICustomizedContentProviderFactory.IContentListener;
import org.eclipse.emf.facet.custom.ui.ICustomizedLabelProvider;
import org.eclipse.emf.facet.custom.ui.IResolvingCustomizedLabelProviderFactory;
import org.eclipse.emf.facet.efacet.core.IFacetManager;
import org.eclipse.emf.facet.efacet.core.IFacetManagerFactory;
import org.eclipse.emf.facet.efacet.core.IFacetManagerListener;
import org.eclipse.emf.facet.efacet.core.IFacetManagerProvider;
import org.eclipse.emf.facet.util.core.Logger;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.views.properties.IPropertySheetPage;

import in.jpep.emf.facet.starterkit.modelset.ModelSet;
import in.jpep.emf.facet.starterkit.modelset.ModelsetFactory;

public class EmfFacetStarterKitEditor extends EditorPart
		implements IEditingDomainProvider, IFacetManagerProvider, ICustomizationManagerProvider, ISelectionProvider {

	private IPropertySheetPage propertySheetPage;
	private EditingDomain editingDomain;
	private IFacetManager facetManager;
	private ICustomizationManager customManager;
	private ICustomizedLabelProvider labelProvider;
	private TreeViewer treeViewer;
	private List<Resource> workingResources = new ArrayList<Resource>();
	private final List<ISelectionChangedListener> listeners = new CopyOnWriteArrayList<ISelectionChangedListener>();
	private ModelSet modelSet;
	private Resource modelSetRsrc;

	@Override
	public void createPartControl(Composite parent) {
		createTreeViewer(parent);
		final IFacetManagerListener fManagerListener = new IFacetManagerListener() {
			public void facetManagerChanged() {
				refresh();
			}
		};
		this.facetManager.addListener(fManagerListener);
		getSite().setSelectionProvider(this);
		final MenuManager menuManager = new MenuManager();
		final Menu contextMenu = menuManager.createContextMenu(this.treeViewer.getControl());
		this.treeViewer.getTree().setMenu(contextMenu);
		getSite().registerContextMenu(menuManager, this.treeViewer);
	}

	@Override
	public void init(final IEditorSite site, final IEditorInput input) throws PartInitException {
		setSite(site);
		setInput(input);
		this.editingDomain = ReflexiveAdapterFactoryUtils.getAdapterFactoryEditingDomain();
		final ResourceSet resourceSet = this.editingDomain.getResourceSet();
		final URI resourceURI = EditUIUtil.getURI(getEditorInput());
		final Resource resource = resourceSet.getResource(resourceURI, true);
		final EObject root = resource.getContents().get(0);
		if (root instanceof ModelSet) {
			this.modelSet = (ModelSet) root;
			this.modelSetRsrc = resource;
			final List<String> modelsUriStr = this.modelSet.getModels();
			for (String modelUriStr : modelsUriStr) {
				final URI modelUri = URI.createURI(modelUriStr);
				final Resource modelRsrc = resourceSet.getResource(modelUri, true);
				this.workingResources.add(modelRsrc);
			}
			final String serialUriStr = this.modelSet.getSerialization();
			if (serialUriStr != null) {
				final URI serialUri = URI.createURI(serialUriStr);
				final Resource serialRsrc = resourceSet.getResource(serialUri, true);
				this.facetManager = IFacetManagerFactory.DEFAULT
						.getOrCreateDefaultFacetManagerWithSerializationResource(serialRsrc);
			}
		} else {
			this.workingResources.add(resource);
			this.facetManager = IFacetManagerFactory.DEFAULT.getOrCreateDefaultFacetManager(resourceSet);
			this.modelSet = ModelsetFactory.eINSTANCE.createModelSet();
			final String uriStr = resource.getURI().toString();
			this.modelSet.getModels().add(uriStr);
		}
		this.customManager = ICustomizationManagerFactory.DEFAULT.getOrCreateICustomizationManager(resourceSet);
		this.labelProvider = IResolvingCustomizedLabelProviderFactory.DEFAULT
				.createCustomizedLabelProvider(this.customManager);
	}

	private void createTreeViewer(final Composite parent) {
		this.treeViewer = new TreeViewer(parent);
		final IContentProvider contentProvider = ICustomizedContentProviderFactory.DEFAULT
				.createCustomizedTreeContentProvider(this.customManager, new IContentListener() {
					public void onUpdate(final Object object) {
						refresh(object);
					}
				});
		this.treeViewer.setContentProvider(contentProvider);
		this.treeViewer.setLabelProvider(this.labelProvider);
		final List<EObject> input = new ArrayList<EObject>();
		for (final Resource resource : this.workingResources) {
			input.addAll(resource.getContents());
		}
		this.treeViewer.setInput(input);
		this.treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(final SelectionChangedEvent event) {
				onSelectionChanged(event);
			}
		});
	}

	protected void refresh(Object object) {
		this.treeViewer.refresh(object);

	}

	protected void refresh() {
		this.treeViewer.refresh();
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		if (this.modelSetRsrc == null) {
			doSaveAs();
		}
		final Map<Object, Object> saveOptions = new HashMap<Object, Object>();
		saveOptions.put(Resource.OPTION_SAVE_ONLY_IF_CHANGED, Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER);
		final List<Resource> resourcesToSave = this.workingResources;
		final WorkspaceModifyOperation operation = new WorkspaceModifyOperation() {
			@Override
			public void execute(IProgressMonitor progMonitor) {
				final EditingDomain editDomain = getEditingDomain();
				for (final Resource rsrc : resourcesToSave) {
					if (!rsrc.getContents().isEmpty() && !editDomain.isReadOnly(rsrc)) {
						try {
							rsrc.save(saveOptions);
						} catch (Exception exc) {
							String message = String.format("Error on save resource '%s'", rsrc); //$NON-NLS-1$
							Logger.logError(exc, message, Activator.getDefault());
						}
					}
				}
				final Resource serialRsrc = getFacetManager().getSerializationResource();
				if (serialRsrc != null) {
					try {
						serialRsrc.save(saveOptions);
					} catch (IOException exc) {
						String message = String.format("Error on save Serial resource '%s'", serialRsrc); //$NON-NLS-1$
						Logger.logError(exc, message, Activator.getDefault());
					}
				}
				try {
					getModelSetRsrc().save(saveOptions);
				} catch (IOException exc) {
					String message = String.format("Error on save ModelSet resource '%s'", serialRsrc); //$NON-NLS-1$
					Logger.logError(exc, message, Activator.getDefault());
				}
			}
		};
		try {
			new ProgressMonitorDialog(getSite().getShell()).run(true, false, operation);
			((BasicCommandStack) this.editingDomain.getCommandStack()).saveIsDone();
			firePropertyChange(IEditorPart.PROP_DIRTY);
		} catch (Exception exc) {
			Logger.logError(exc, "Error on save", Activator.getDefault()); //$NON-NLS-1$
		}
	}

	@Override
	public void doSaveAs() {
		final Shell shell = getEditorSite().getShell();
		final ResourceDialog rsrcDialog = new ResourceDialog(shell,
				Messages.EmfFacetStarterKitEditor_SaveModelSetDialogTitle, SWT.SAVE) {

			@Override
			protected boolean processResources() {
				final List<URI> urIs = getURIs();
				final URI modelSetUri = urIs.get(0);
				if (modelSetUri != null) {
					ResourceSet resourceSet = getEditingDomain().getResourceSet();
					setModelSetRsrc(resourceSet.createResource(modelSetUri));
					doSave(new NullProgressMonitor());
				}
				return true;
			}
		};
		rsrcDialog.open();
	}

	protected void setModelSetRsrc(final Resource modelSetRsrc) {
		this.modelSetRsrc = modelSetRsrc;
		this.modelSetRsrc.getContents().add(this.modelSet);
	}

	@Override
	public boolean isDirty() {
		final BasicCommandStack commandStack = (BasicCommandStack) this.editingDomain.getCommandStack();
		return commandStack.isSaveNeeded() || this.modelSetRsrc == null
				|| (this.modelSetRsrc != null && this.modelSetRsrc.isModified());
	}

	@Override
	public boolean isSaveAsAllowed() {
		return true;
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

	public IPropertySheetPage getPropertySheetPage() {
		if (this.propertySheetPage == null) {
			this.propertySheetPage = IEditingDomainPropertySheetPageFactory.DEFAULT.createPropertySheetPage(this);
		}
		return this.propertySheetPage;
	}

	@SuppressWarnings("rawtypes")
	/*
	 * @SuppressWarnings("rawtypes") > jpepin, imposed by interface
	 */
	@Override
	public Object getAdapter(Class adapter) {
		Object result;
		if (adapter.isInstance(this)) {
			result = this;
		} else if (adapter.equals(IPropertySheetPage.class)) {
			result = getPropertySheetPage();
		} else if (adapter.equals(TreeViewer.class)) {
			result = this.treeViewer;
		} else if (adapter.equals(ModelSet.class)) {
			result = this.modelSet;
		} else {
			result = super.getAdapter(adapter);
		}
		return result;
	}

	public IFacetManager getFacetManager() {
		return this.facetManager;
	}

	public EditingDomain getEditingDomain() {
		return this.editingDomain;
	}

	public ICustomizationManager getCustomizationManager() {
		return this.customManager;
	}

	public void addSelectionChangedListener(ISelectionChangedListener listener) {
		this.listeners.add(listener);
	}

	public void removeSelectionChangedListener(ISelectionChangedListener listener) {
		this.listeners.remove(listener);
	}

	public void setSelection(ISelection selection) {
		this.treeViewer.setSelection(selection);
	}

	public ISelection getSelection() {
		final ISelection selection = this.treeViewer.getSelection();
		return CustomizedContentProviderUtils.resolveSelection(selection);
	}

	protected void onSelectionChanged(SelectionChangedEvent event) {
		final ISelection selection = event.getSelection();
		final ISelection resolvedSelection = CustomizedContentProviderUtils.resolveSelection(selection);
		final SelectionChangedEvent resolvedEvent = new SelectionChangedEvent(this, resolvedSelection);
		for (final ISelectionChangedListener listener : this.listeners) {
			listener.selectionChanged(resolvedEvent);
		}
	}

	protected Resource getModelSetRsrc() {
		return this.modelSetRsrc;
	}

	public void loadResources(final List<URI> uris) {
		for (URI uri : uris) {
			final Resource newRsrc = this.editingDomain.getResourceSet().getResource(uri, true);
			final String uriStr = uri.toString();
			this.modelSet.getModels().add(uriStr);
			this.workingResources.add(newRsrc);
		}
		final List<EObject> input = new ArrayList<EObject>();
		for (final Resource rsrc : this.workingResources) {
			final List<EObject> contents = rsrc.getContents();
			input.addAll(contents);
		}
		this.treeViewer.setInput(input);
	}

}
