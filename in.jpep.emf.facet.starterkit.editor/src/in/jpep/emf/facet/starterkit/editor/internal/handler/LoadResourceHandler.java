/**
 * Copyright (c) 2017 Jonathan Pepin
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Jonathan Pepin - initial API and implementation
 */
package in.jpep.emf.facet.starterkit.editor.internal.handler;

import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.emf.edit.ui.action.LoadResourceAction.LoadResourceDialog;
import org.eclipse.emf.facet.util.ui.internal.exported.handler.HandlerUtils;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;

import in.jpep.emf.facet.starterkit.editor.internal.EmfFacetStarterKitEditor;

public class LoadResourceHandler extends AbstractHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {
		final IWorkbenchPart activePart = HandlerUtils.getActivePart();
		if (activePart instanceof IEditingDomainProvider) {
			final IEditingDomainProvider editingDomainP = (IEditingDomainProvider) activePart;
			final EditingDomain editingDomain = editingDomainP.getEditingDomain();
			final Shell shell = activePart.getSite().getShell();
			final LoadResourceDialog dialog = new LoadResourceDialog(shell, editingDomain) {
				@Override
				protected boolean processResources() {
					final List<URI> uris = getURIs();
					loadResources(uris, activePart);
					return true;
				}
			};
			dialog.open();
		}
		return null;
	}

	protected static void loadResources(final List<URI> uris, final IWorkbenchPart activePart) {
		if (activePart instanceof EmfFacetStarterKitEditor) {
			final EmfFacetStarterKitEditor editor = (EmfFacetStarterKitEditor) activePart;
			editor.loadResources(uris);
		}
	}

}
