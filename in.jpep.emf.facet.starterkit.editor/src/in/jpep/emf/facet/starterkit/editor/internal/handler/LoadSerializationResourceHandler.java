/**
 * Copyright (c) 2017 Jonathan Pepin
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Jonathan Pepin - initial API and implementation
 */
package in.jpep.emf.facet.starterkit.editor.internal.handler;

import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.ui.action.LoadResourceAction.LoadResourceDialog;
import org.eclipse.emf.facet.efacet.core.IFacetManager;
import org.eclipse.emf.facet.efacet.core.IFacetManagerProvider;
import org.eclipse.emf.facet.efacet.core.exception.FacetManagerException;
import org.eclipse.emf.facet.util.ui.internal.exported.dialog.IOkDialogFactory;
import org.eclipse.emf.facet.util.ui.internal.exported.handler.HandlerUtils;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;

import in.jpep.emf.facet.starterkit.modelset.ModelSet;

public class LoadSerializationResourceHandler extends AbstractHandler {

	@Override
	public boolean isEnabled() {
		return isFacetManagerHandled();
	}

	@Override
	public boolean isHandled() {
		return isFacetManagerHandled();
	}

	private static boolean isFacetManagerHandled() {
		return HandlerUtils.getActivePart() instanceof IFacetManagerProvider;
	}

	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final IWorkbenchPart activePart = HandlerUtils.getActivePart();
		final ModelSet modelSet = (ModelSet) activePart.getAdapter(ModelSet.class);
		final IFacetManagerProvider facetManagerP = (IFacetManagerProvider) activePart;
		final IFacetManager facetManager = facetManagerP.getFacetManager();
		if (facetManager != null) {
			final IWorkbenchPartSite site = activePart.getSite();
			final Shell shell = site.getShell();
			final LoadResourceDialog dialog = new LoadSerializationDialog(shell, facetManager, modelSet);
			dialog.open();
		}
		return null;
	}

	class LoadSerializationDialog extends LoadResourceDialog {

		private final IFacetManager facetManager;
		private final ModelSet modelSet;

		public LoadSerializationDialog(final Shell parent, final IFacetManager facetManager, final ModelSet modelSet) {
			super(parent);
			this.facetManager = facetManager;
			this.modelSet = modelSet;
		}

		@Override
		protected Control createDialogArea(Composite parent) {
			final Control createdDialogArea = super.createDialogArea(parent);
			final URI serialRsrcUri = getSerialResourceUri(this.facetManager);
			if (serialRsrcUri != null) {
				this.uriField.setText(serialRsrcUri.toString());
			}
			return createdDialogArea;
		}

		@Override
		protected boolean processResources() {
			final List<URI> urIs = getURIs();
			final URI serialUri = urIs.get(0);
			changeSerializationURI(this.facetManager, serialUri, this.modelSet);
			return true;
		}

	}

	protected static URI getSerialResourceUri(final IFacetManager facetManager) {
		final Resource serialResource = facetManager.getSerializationResource();
		URI uri = null;
		if (serialResource != null) {
			uri = serialResource.getURI();
		}
		return uri;
	}

	protected static void changeSerializationURI(final IFacetManager facetManager, final URI uri,
			final ModelSet modelSet) {
		Resource sResource = null;
		final ResourceSet resourceSet = facetManager.getResourceSet();
		try {
			sResource = resourceSet.getResource(uri, true);
		} catch (@SuppressWarnings("unused") Exception exc) {
			/*
			 * @SuppressWarnings("unused") > jpepin, catched by creation instead of loading
			 */
			sResource = resourceSet.createResource(uri);
		}
		try {
			facetManager.setSerializationResource(sResource);
			final String uriStr = sResource.getURI().toString();
			modelSet.setSerialization(uriStr);
		} catch (FacetManagerException e) {
			IOkDialogFactory.DEFAULT.openErrorDialog(new Shell(), e, "Error on change serialization resource"); //$NON-NLS-1$
		}
	}

}