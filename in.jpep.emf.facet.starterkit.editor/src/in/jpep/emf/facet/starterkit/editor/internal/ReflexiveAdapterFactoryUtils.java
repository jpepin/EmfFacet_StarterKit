/**
 * Copyright (c) 2017 Jonathan Pepin
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Jonathan Pepin - initial API and implementation
 */
package in.jpep.emf.facet.starterkit.editor.internal;

import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.resource.ResourceItemProviderAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.swt.graphics.Image;

public class ReflexiveAdapterFactoryUtils {

	public static EditingDomain getAdapterFactoryEditingDomain() {
		final BasicCommandStack commandStack = new BasicCommandStack();
		final AdapterFactory adapterFactory = getReflexiveAdapterFactory();
		return new AdapterFactoryEditingDomain(adapterFactory, commandStack);
	}

	public static ILabelProvider getLabelProvider() {
		return new AdapterFactoryLabelProvider(getReflexiveAdapterFactory());
	}

	public static Image getImage(final Object element) {
		final ILabelProvider labelProvider = getLabelProvider();
		return labelProvider.getImage(element);
	}

	public static IItemLabelProvider getIItemLabelProvider(final Object object) {
		final AdapterFactory adapterFactory = getReflexiveAdapterFactory();
		return (IItemLabelProvider) adapterFactory.adapt(object, IItemLabelProvider.class);
	}

	private static AdapterFactory getReflexiveAdapterFactory() {
		final ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
				ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		adapterFactory.addAdapterFactory(new ResourceItemProviderAdapterFactory());
		adapterFactory.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());
		return adapterFactory;
	}
}
