/**
 * Copyright (c) 2017 Jonathan Pepin
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Jonathan Pepin - initial API and implementation
 */
package in.jpep.emf.facet.starterkit.editor.internal.handler;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.CreateChildCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.ui.provider.ExtendedImageRegistry;
import org.eclipse.emf.facet.util.ui.internal.exported.handler.HandlerUtils;
import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.ui.IWorkbenchPart;

import in.jpep.emf.facet.starterkit.editor.internal.Messages;
import in.jpep.emf.facet.starterkit.editor.internal.ReflexiveAdapterFactoryUtils;

public class CreateChildContributionItem extends ContributionItem {

	@Override
	public void fill(Menu menu, int index) {
		final IStructuredSelection selection = HandlerUtils.getStructuredSelection();
		if (selection != null) {
			final Object firstElement = selection.getFirstElement();
			final List<?> elements = selection.toList();
			final IWorkbenchPart activePart = HandlerUtils.getActivePart();
			if (activePart instanceof IEditingDomainProvider) {
				final IEditingDomainProvider editingDomainP = (IEditingDomainProvider) activePart;
				final EditingDomain editingDomain = editingDomainP.getEditingDomain();
				final Collection<?> newChildDescriptors = editingDomain.getNewChildDescriptors(firstElement, null);
				final Collection<?> newSiblingDescriptors = editingDomain.getNewChildDescriptors(null, firstElement);
				final IItemLabelProvider itemLabelProvider = ReflexiveAdapterFactoryUtils
						.getIItemLabelProvider(firstElement);
				if (itemLabelProvider instanceof CreateChildCommand.Helper) {
					final CreateChildCommand.Helper helper = (CreateChildCommand.Helper) itemLabelProvider;
					createMenuDescriptors(menu, elements, editingDomain, newChildDescriptors, helper,
							Messages.CreateChildContributionItem_CreateChild);
					createMenuDescriptors(menu, elements, editingDomain, newSiblingDescriptors, helper,
							Messages.CreateChildContributionItem_CreateSibling);
				}
			}
		}
	}

	private static void createMenuDescriptors(Menu menu, final List<?> elements, final EditingDomain editingDomain,
			final Collection<?> newChildDescriptors, final CreateChildCommand.Helper helper, String baseLabel) {
		for (Object descriptor : newChildDescriptors) {
			final CommandParameter commandParameter = (CommandParameter) descriptor;
			final MenuItem menuItem = new MenuItem(menu, SWT.PUSH);
			final EStructuralFeature feature = (EStructuralFeature) commandParameter.getFeature();
			final Object child = commandParameter.getValue();
			final EObject owner = ((EObject) commandParameter.getFeature()).eContainer();
			final String childText = helper.getCreateChildText(owner, feature, child, null);
			final String label = NLS.bind(baseLabel, childText);
			menuItem.setText(label);
			final Object childImg = helper.getCreateChildImage(owner, feature, child, null);
			final ImageDescriptor imgDesc = ExtendedImageRegistry.getInstance().getImageDescriptor(childImg);
			final Image image = imgDesc.createImage();
			menuItem.setImage(image);
			menuItem.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					final CreateChildCommand cmd = new CreateChildCommand(editingDomain, owner, feature, child,
							elements);
					cmd.execute();
				}
			});
		}
	}

}
