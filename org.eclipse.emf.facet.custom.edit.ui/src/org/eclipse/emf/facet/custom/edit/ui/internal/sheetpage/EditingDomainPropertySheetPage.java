/*******************************************************************************
 * Copyright (c) 2015 Soft-Maint.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Jonathan Pepin (Soft-Maint) - Bug 480490 - PropertySheetPage Factory from editingdomain
 *******************************************************************************/

package org.eclipse.emf.facet.custom.edit.ui.internal.sheetpage;

import java.util.List;

import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.emf.edit.ui.action.EditingDomainActionBarContributor;
import org.eclipse.emf.edit.ui.view.ExtendedPropertySheetPage;
import org.eclipse.emf.facet.custom.core.ICustomizationManager;
import org.eclipse.emf.facet.custom.core.ICustomizationManagerProvider;
import org.eclipse.emf.facet.custom.ui.ICustomizedLabelProvider;
import org.eclipse.emf.facet.custom.ui.ICustomizedLabelProviderFactory;
import org.eclipse.emf.facet.efacet.core.IFacetManager;
import org.eclipse.emf.facet.efacet.core.IFacetManagerProvider;
import org.eclipse.emf.facet.efacet.edit.ui.IFacetPropertySourceProviderFactory;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorActionBarContributor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.views.properties.IPropertySourceProvider;

public class EditingDomainPropertySheetPage extends ExtendedPropertySheetPage {

	private final IEditorPart part;

	public EditingDomainPropertySheetPage(final IEditorPart part) {
		this(getEditingDomain(part), getFacetManager(part),
				getCustomManager(part), part);
	}

	private static ICustomizationManager getCustomManager(final IEditorPart part) {
		final ICustomizationManagerProvider customMgrP =
			(ICustomizationManagerProvider) part.getAdapter(
				ICustomizationManagerProvider.class);
		return customMgrP.getCustomizationManager();
	}

	private static IFacetManager getFacetManager(final IEditorPart part) {
		final IFacetManagerProvider customMgrP = (IFacetManagerProvider) part
				.getAdapter(IFacetManagerProvider.class);
		return customMgrP.getFacetManager();
	}

	private static AdapterFactoryEditingDomain getEditingDomain(
			final IEditorPart part) {
		final IEditingDomainProvider eDomainP = (IEditingDomainProvider) part
				.getAdapter(IEditingDomainProvider.class);
		final EditingDomain editingDomain = eDomainP.getEditingDomain();
		if (!(editingDomain instanceof AdapterFactoryEditingDomain)) {
			final String msg = String.format(
					"EditingDomain must me instance of %s instead of %s !", //$NON-NLS-1$
					AdapterFactoryEditingDomain.class.getName(), eDomainP
							.getClass().getName());
			throw new IllegalStateException(msg);
		}
		return (AdapterFactoryEditingDomain) editingDomain;
	}

	public EditingDomainPropertySheetPage(
			final AdapterFactoryEditingDomain editingDomain,
			final IFacetManager facetManager,
			final ICustomizationManager customManager, final IEditorPart part) {
		super(editingDomain);
		this.part = part;
		final ICustomizedLabelProvider labelProvider =
				ICustomizedLabelProviderFactory.DEFAULT
						.createCustomizedLabelProvider(customManager);
		final IPropertySourceProvider provider =
			IFacetPropertySourceProviderFactory.DEFAULT
				.createPropertySourceProvider(
						editingDomain.getAdapterFactory(), this.editingDomain,
						labelProvider, facetManager);
		this.setPropertySourceProvider(provider);
	}

	@Override
	public void setSelectionToViewer(final List<?> selection) {
		this.part.setFocus();
	}

	@Override
	public void setActionBars(final IActionBars actionBars) {
		super.setActionBars(actionBars);
		final IEditorSite editorSite = this.part.getEditorSite();
		final IEditorActionBarContributor actionBarContrib = editorSite
				.getActionBarContributor();
		if (actionBarContrib instanceof EditingDomainActionBarContributor) {
			final EditingDomainActionBarContributor edActBarContrib =
					(EditingDomainActionBarContributor) editorSite;
			edActBarContrib.shareGlobalActions(this, actionBars);
		}
	}

}
