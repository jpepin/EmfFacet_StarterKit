/*******************************************************************************
 * Copyright (c) 2015 Soft-Maint.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Jonathan Pepin (Soft-Maint) - Bug 480490 - PropertySheetPage Factory from editingdomain
 *******************************************************************************/

package org.eclipse.emf.facet.custom.edit.ui.internal.sheetpage;

import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.facet.custom.core.ICustomizationManager;
import org.eclipse.emf.facet.custom.edit.ui.IEditingDomainPropertySheetPageFactory;
import org.eclipse.emf.facet.efacet.core.IFacetManager;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.views.properties.IPropertySheetPage;

public class EditingDomainPropertySheetPageFactory implements
		IEditingDomainPropertySheetPageFactory {

	public IPropertySheetPage createPropertySheetPage(
			final IEditorPart editorPart) {
		return new EditingDomainPropertySheetPage(editorPart);
	}

	public IPropertySheetPage createPropertySheetPage(
			final AdapterFactoryEditingDomain editingDomain,
			final IFacetManager facetManager,
			final ICustomizationManager customManager, final IEditorPart part) {
		return new EditingDomainPropertySheetPage(editingDomain, facetManager,
				customManager, part);
	}

}
