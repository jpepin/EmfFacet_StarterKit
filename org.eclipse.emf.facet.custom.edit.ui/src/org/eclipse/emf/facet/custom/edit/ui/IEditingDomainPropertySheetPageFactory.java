/*******************************************************************************
 * Copyright (c) 2015 Soft-Maint.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Jonathan Pepin (Soft-Maint) - Bug 480490 - PropertySheetPage Factory from editingdomain
 *******************************************************************************/

package org.eclipse.emf.facet.custom.edit.ui;

import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.facet.custom.core.ICustomizationManager;
import org.eclipse.emf.facet.custom.edit.ui.internal.sheetpage.EditingDomainPropertySheetPage;
import org.eclipse.emf.facet.custom.edit.ui.internal.sheetpage.EditingDomainPropertySheetPageFactory;
import org.eclipse.emf.facet.efacet.core.IFacetManager;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.views.properties.IPropertySheetPage;

/**
 * Factory for {@link EditingDomainPropertySheetPage}.
 *
 * @noimplement This interface is not intended to be implemented by clients.
 * @noextend This interface is not intended to be extended by clients.
 */
public interface IEditingDomainPropertySheetPageFactory {

	IEditingDomainPropertySheetPageFactory DEFAULT =
			new EditingDomainPropertySheetPageFactory();

	IPropertySheetPage createPropertySheetPage(IEditorPart editorPart);

	IPropertySheetPage createPropertySheetPage(
			AdapterFactoryEditingDomain editingDomain,
			IFacetManager facetManager, ICustomizationManager customManager,
			IEditorPart part);

}
