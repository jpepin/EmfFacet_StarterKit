/**
 * Copyright (c) 2017 Jonathan Pepin
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Jonathan Pepin - initial API and implementation
 */
package in.jpep.emf.facet.starterkit.facetset.internal.queries;

import org.eclipse.emf.facet.custom.metamodel.custompt.IImage;
import org.eclipse.emf.facet.custom.ui.ImageUtils;
import org.eclipse.emf.facet.efacet.core.IFacetManager;
import org.eclipse.emf.facet.efacet.core.exception.DerivedTypedElementException;
import org.eclipse.emf.facet.query.java.core.IJavaQuery2;
import org.eclipse.emf.facet.query.java.core.IParameterValueList2;
import org.eclipse.emf.facet.util.swt.imageprovider.IImageProvider;
import org.eclipse.emf.facet.util.swt.imageprovider.IImageProviderFactory;
import org.eclipse.swt.graphics.Image;

import in.jpep.emf.facet.starterkit.facetset.internal.Activator;
import in.jpep.emf.facet.starterkit.human.Gender;
import in.jpep.emf.facet.starterkit.human.Human;

public class GenderImageQuery implements IJavaQuery2<Human, IImage> {

	private static final IImageProvider IMG_P = IImageProviderFactory.DEFAULT
			.createIImageProvider(Activator.getDefault());
	private static final Image WOMEN_IMG = IMG_P.getImage("icons/women.gif"); //$NON-NLS-1$
	private static final Image MEN_IMG = IMG_P.getImage("icons/men.gif"); //$NON-NLS-1$
	private static final IImage WOMEN_IIMG = ImageUtils.wrap(WOMEN_IMG);
	private static final IImage MEN_IIMG = ImageUtils.wrap(MEN_IMG);

	public static IImage getImage(final Gender gender) {
		IImage result;
		switch (gender) {
		case WOMEN:
			result = WOMEN_IIMG;
			break;
		case MEN:
		default:
			result = MEN_IIMG;
			break;
		}
		return result;
	}

	@Override
	public IImage evaluate(final Human source, final IParameterValueList2 parameterValues,
			final IFacetManager facetManager) throws DerivedTypedElementException {
		final Gender gender = source.getGender();
		return getImage(gender);
	}

}
