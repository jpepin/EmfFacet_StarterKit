/**
 */
package in.jpep.emf.facet.starterkit.city;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>World</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link in.jpep.emf.facet.starterkit.city.World#getLocations <em>Locations</em>}</li>
 * </ul>
 *
 * @see in.jpep.emf.facet.starterkit.city.CityPackage#getWorld()
 * @model
 * @generated
 */
public interface World extends EObject {
	/**
	 * Returns the value of the '<em><b>Locations</b></em>' containment reference list.
	 * The list contents are of type {@link in.jpep.emf.facet.starterkit.city.City}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locations</em>' containment reference list.
	 * @see in.jpep.emf.facet.starterkit.city.CityPackage#getWorld_Locations()
	 * @model containment="true"
	 * @generated
	 */
	EList<City> getLocations();

} // World
