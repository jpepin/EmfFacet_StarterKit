/**
 */
package in.jpep.emf.facet.starterkit.human;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>People</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link in.jpep.emf.facet.starterkit.human.People#getPopulation <em>Population</em>}</li>
 * </ul>
 *
 * @see in.jpep.emf.facet.starterkit.human.HumanPackage#getPeople()
 * @model
 * @generated
 */
public interface People extends EObject {
	/**
	 * Returns the value of the '<em><b>Population</b></em>' containment reference list.
	 * The list contents are of type {@link in.jpep.emf.facet.starterkit.human.Human}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Population</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Population</em>' containment reference list.
	 * @see in.jpep.emf.facet.starterkit.human.HumanPackage#getPeople_Population()
	 * @model containment="true"
	 * @generated
	 */
	EList<Human> getPopulation();

} // People
