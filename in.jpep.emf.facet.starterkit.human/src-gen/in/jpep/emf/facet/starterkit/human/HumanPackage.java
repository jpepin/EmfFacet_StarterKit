/**
 */
package in.jpep.emf.facet.starterkit.human;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see in.jpep.emf.facet.starterkit.human.HumanFactory
 * @model kind="package"
 * @generated
 */
public interface HumanPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "human";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.jpep.in/human";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "human";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	HumanPackage eINSTANCE = in.jpep.emf.facet.starterkit.human.impl.HumanPackageImpl.init();

	/**
	 * The meta object id for the '{@link in.jpep.emf.facet.starterkit.human.impl.HumanImpl <em>Human</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see in.jpep.emf.facet.starterkit.human.impl.HumanImpl
	 * @see in.jpep.emf.facet.starterkit.human.impl.HumanPackageImpl#getHuman()
	 * @generated
	 */
	int HUMAN = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HUMAN__NAME = 0;

	/**
	 * The feature id for the '<em><b>Gender</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HUMAN__GENDER = 1;

	/**
	 * The feature id for the '<em><b>Age</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HUMAN__AGE = 2;

	/**
	 * The number of structural features of the '<em>Human</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HUMAN_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Human</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HUMAN_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link in.jpep.emf.facet.starterkit.human.impl.PeopleImpl <em>People</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see in.jpep.emf.facet.starterkit.human.impl.PeopleImpl
	 * @see in.jpep.emf.facet.starterkit.human.impl.HumanPackageImpl#getPeople()
	 * @generated
	 */
	int PEOPLE = 1;

	/**
	 * The feature id for the '<em><b>Population</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PEOPLE__POPULATION = 0;

	/**
	 * The number of structural features of the '<em>People</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PEOPLE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>People</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PEOPLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link in.jpep.emf.facet.starterkit.human.Gender <em>Gender</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see in.jpep.emf.facet.starterkit.human.Gender
	 * @see in.jpep.emf.facet.starterkit.human.impl.HumanPackageImpl#getGender()
	 * @generated
	 */
	int GENDER = 2;

	/**
	 * Returns the meta object for class '{@link in.jpep.emf.facet.starterkit.human.Human <em>Human</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Human</em>'.
	 * @see in.jpep.emf.facet.starterkit.human.Human
	 * @generated
	 */
	EClass getHuman();

	/**
	 * Returns the meta object for the attribute '{@link in.jpep.emf.facet.starterkit.human.Human#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see in.jpep.emf.facet.starterkit.human.Human#getName()
	 * @see #getHuman()
	 * @generated
	 */
	EAttribute getHuman_Name();

	/**
	 * Returns the meta object for the attribute '{@link in.jpep.emf.facet.starterkit.human.Human#getGender <em>Gender</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Gender</em>'.
	 * @see in.jpep.emf.facet.starterkit.human.Human#getGender()
	 * @see #getHuman()
	 * @generated
	 */
	EAttribute getHuman_Gender();

	/**
	 * Returns the meta object for the attribute '{@link in.jpep.emf.facet.starterkit.human.Human#getAge <em>Age</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Age</em>'.
	 * @see in.jpep.emf.facet.starterkit.human.Human#getAge()
	 * @see #getHuman()
	 * @generated
	 */
	EAttribute getHuman_Age();

	/**
	 * Returns the meta object for class '{@link in.jpep.emf.facet.starterkit.human.People <em>People</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>People</em>'.
	 * @see in.jpep.emf.facet.starterkit.human.People
	 * @generated
	 */
	EClass getPeople();

	/**
	 * Returns the meta object for the containment reference list '{@link in.jpep.emf.facet.starterkit.human.People#getPopulation <em>Population</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Population</em>'.
	 * @see in.jpep.emf.facet.starterkit.human.People#getPopulation()
	 * @see #getPeople()
	 * @generated
	 */
	EReference getPeople_Population();

	/**
	 * Returns the meta object for enum '{@link in.jpep.emf.facet.starterkit.human.Gender <em>Gender</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Gender</em>'.
	 * @see in.jpep.emf.facet.starterkit.human.Gender
	 * @generated
	 */
	EEnum getGender();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	HumanFactory getHumanFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link in.jpep.emf.facet.starterkit.human.impl.HumanImpl <em>Human</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see in.jpep.emf.facet.starterkit.human.impl.HumanImpl
		 * @see in.jpep.emf.facet.starterkit.human.impl.HumanPackageImpl#getHuman()
		 * @generated
		 */
		EClass HUMAN = eINSTANCE.getHuman();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HUMAN__NAME = eINSTANCE.getHuman_Name();

		/**
		 * The meta object literal for the '<em><b>Gender</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HUMAN__GENDER = eINSTANCE.getHuman_Gender();

		/**
		 * The meta object literal for the '<em><b>Age</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HUMAN__AGE = eINSTANCE.getHuman_Age();

		/**
		 * The meta object literal for the '{@link in.jpep.emf.facet.starterkit.human.impl.PeopleImpl <em>People</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see in.jpep.emf.facet.starterkit.human.impl.PeopleImpl
		 * @see in.jpep.emf.facet.starterkit.human.impl.HumanPackageImpl#getPeople()
		 * @generated
		 */
		EClass PEOPLE = eINSTANCE.getPeople();

		/**
		 * The meta object literal for the '<em><b>Population</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PEOPLE__POPULATION = eINSTANCE.getPeople_Population();

		/**
		 * The meta object literal for the '{@link in.jpep.emf.facet.starterkit.human.Gender <em>Gender</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see in.jpep.emf.facet.starterkit.human.Gender
		 * @see in.jpep.emf.facet.starterkit.human.impl.HumanPackageImpl#getGender()
		 * @generated
		 */
		EEnum GENDER = eINSTANCE.getGender();

	}

} //HumanPackage
